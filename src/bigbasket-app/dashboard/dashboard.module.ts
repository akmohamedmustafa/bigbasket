import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { DashboardComponent } from "./dashboard.component";
import { HomeComponent } from "./home.component";
import { DashboardRoutingModule } from "../routings/dashboard-routing";
import { BaseLogger, FileLogger, DBLogger, ConsoleLogger } from '../utilities/ilogger.interface';

const provideColl: any = []; 
// coming from let say: service http call
provideColl.push({ provide: BaseLogger, useClass: DBLogger });
provideColl.push({ provide: 'token1', useClass: FileLogger });
provideColl.push({ provide: 'token2', useClass: ConsoleLogger });

@NgModule({    
    declarations: [ DashboardComponent, HomeComponent ],
    imports: [ BrowserModule, DashboardRoutingModule ],
    providers: [ provideColl
        // { provide: BaseLogger, useClass: DBLogger }, // Centralized Provider
        // { provide: '1', useClass: FileLogger }, // Conditional Provider
        // { provide: '2', useClass: ConsoleLogger } // Conditional Provider
    ],
    bootstrap: [ DashboardComponent ]
})

export class DashboardModule {}