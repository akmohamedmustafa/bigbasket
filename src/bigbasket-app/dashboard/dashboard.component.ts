import { Component, OnInit } from "@angular/core";

import { Dashboard } from "./dashboard.model";

@Component ({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.view.html',
    styleUrls: ['dashboard.presentation.scss']
})
export class DashboardComponent implements OnInit {
    dashboardTitle:string = "buy online";
    dashboardInfo: Dashboard = new Dashboard();

    constructor() {}

    ngOnInit() {
        this.dashboardInfo.DashboardCode = 'D-1001';
        this.dashboardInfo.DashboardName = 'Main Dashboard';
        this.dashboardInfo.DashboardAmount = 960;
    };
}