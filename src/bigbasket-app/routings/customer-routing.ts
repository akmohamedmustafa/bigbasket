import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CustomerComponent } from "../customer/customer.component";

const custroutes: Routes = [
    { path: 'add', component: CustomerComponent }
];

@NgModule({
    imports: [RouterModule.forChild(custroutes)],
    exports: [RouterModule]
})
export class CustomerRoutingModule {}