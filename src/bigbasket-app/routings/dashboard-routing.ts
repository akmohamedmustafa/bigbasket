import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from '../dashboard/home.component';

const dashroutes: Routes = [
    { path:'', component: HomeComponent },
    { path:'customer', loadChildren: () => import('./customer-routing').then(m => m.CustomerRoutingModule) },
    { path: 'supplier', loadChildren: ()=>import('./supplier-routing').then(m => m.SupplierRoutingModule) },
    { path: '', redirectTo:'', pathMatch: 'full'}
];

@NgModule({
    imports: [RouterModule.forRoot(dashroutes)],
    exports: [RouterModule ]
})
export class DashboardRoutingModule {}