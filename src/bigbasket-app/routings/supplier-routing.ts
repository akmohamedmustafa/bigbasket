import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { SupplierComponent } from "../supplier/supplier.component";

const splrroutes: Routes = [
    { path: 'add', component: SupplierComponent }
];

@NgModule({
    imports: [RouterModule.forChild(splrroutes)],
    exports: [RouterModule]
})
export class SupplierRoutingModule {}