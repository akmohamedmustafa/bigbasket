import { Component, OnInit } from "@angular/core";

import { BaseLogger } from "../utilities/ilogger.interface";

import { Customer } from "./customer.model";

@Component ({
    selector: 'app-customer',
    templateUrl: 'customer.view.html',
    styleUrls: []
})
export class CustomerComponent implements OnInit {
    custTitle:string = "Customer Information";
    custInfo: Customer = new Customer();
    LogObj: BaseLogger = null;
    constructor(_logger: BaseLogger) {
        this.LogObj = _logger;
        this.LogObj.Log();
    }

    ngOnInit() {
        this.custInfo.CustomerCode = '1001';
        this.custInfo.CustomerName = 'Adam';
        this.custInfo.CustomerAmount = 960;
    };
}