import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CustomerComponent } from "./customer.component";
import { CustomerRoutingModule } from "../routings/customer-routing";

@NgModule({    
    declarations: [ CustomerComponent ],
    imports: [ CommonModule, CustomerRoutingModule ],
    providers: [],
    bootstrap: [ CustomerComponent ]
})

export class CustomerModule {}