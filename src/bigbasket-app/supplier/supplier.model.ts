export class Supplier {
    SupplierCode: string;
    SupplierName: string;
    SupplierAmount: number;
}