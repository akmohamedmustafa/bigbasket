import { Component, OnInit, Injector } from "@angular/core";

import { BaseLogger } from "../utilities/ilogger.interface";

import { Supplier } from "./supplier.model";

@Component ({
    selector: 'app-supplier',
    templateUrl: 'supplier.view.html',
    styleUrls: []
})
export class SupplierComponent implements OnInit {
    splrTitle:string = "Supplier Information";
    splrInfo: Supplier = new Supplier();

    LogObj: BaseLogger = null;
    constructor(_injector: Injector) {
        this.LogObj = _injector.get('token2');
        this.LogObj.Log();
    }

    ngOnInit() {
        this.splrInfo.SupplierCode = 'S-1001';
        this.splrInfo.SupplierName = 'Almarai';
        this.splrInfo.SupplierAmount = 10000;
    };
}