import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SupplierComponent } from "./supplier.component";
import { SupplierRoutingModule } from "../routings/supplier-routing";

@NgModule({    
    declarations: [ SupplierComponent ],
    imports: [ CommonModule, SupplierRoutingModule ],
    providers: [],
    bootstrap: [ SupplierComponent ]
})

export class SupplierModule {}